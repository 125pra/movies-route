import React, { Component } from "react";
import "./App.css";
import { Route } from "react-router-dom";
import axios from "axios";
import NavBar from "./component/navbar";
import Directors from "./component/directors";
import Movies from "./component/movies";
import { array } from "prop-types";

const directorApiEndPoint = "http://localhost:4500/api/directors";

class App extends Component {
  state = {
    directorsData: [],
    searchWord: "",
    addDirector: ""
  };

  handleSearchChange = event => {
    let searchWord = event.target.value;
    this.setState({ searchWord });
  };

  //  componentDidMount() {
  //    fetch("http://localhost:4500/api/directors")
  //      .then(res => res.json())
  //      .then(result => {
  //        this.setState({ directorsData: result });
  //      });
  //  }

  async componentDidMount() {
    let { data: directorsData } = await axios.get(directorApiEndPoint);
    this.setState({ directorsData });
  }

  handleAddChange = event => {
    let addDirector = event.target.value;
    this.setState({ addDirector });
  };

  handleSearch = () => {
    const directorData = this.state.directorsData.filter(director =>
      director.name.toLowerCase().includes(this.state.searchWord.toLowerCase())
    );
    return directorData;
  };

  //   handleSubmit=()=>{
  //      try{
  //         fetch("http://localhost:4500/api/directors", {
  //           method: 'POST',
  //           headers: {
  //             'Accept': 'application/json, text/plain, */*',
  //             'Content-Type': 'application/json'
  //           },
  //            body: JSON.stringify({name:this.state.addDirector})
  //        }).then(res => res.json()).then(result=>{ this.setState({ });
  //       })
  //    }catch (error) {
  //      console.log(error)
  //  }
  // }

  handleSubmit = async () => {
    // let directorsData=this.state.directorsData;
    // let arrayOfId = directorsData.map(singleData => singleData.directorId);
    //     let maxId = Math.max(...arrayOfId);
    // directorsData=[addDirector, ...directorsData]
    let addDirector = { name: this.state.addDirector };
    await axios.post(directorApiEndPoint, addDirector);
    await this.componentDidMount();
    addDirector = "";
    this.setState({ addDirector });
  };
  deleteDirector = async (directorId, history) => {
    const orignalDirectorsDatt = this.state.directorsData;
    let directorsData = this.state.directorsData;
    directorsData = directorsData.filter(
      singleData => singleData.directorId !== directorId
    );
    this.setState({ directorsData });
    history.push("/directors");
    try {
      await axios.delete(directorApiEndPoint + "/" + directorId);
    } catch (err) {
      console.log("somthing went wrong");
      this.setState({ directorsData: orignalDirectorsDatt });
    }
  };

  submitEdit = async (name, directorId, history) => {
    console.log(name);
    const directorName = name;
    await axios.put(directorApiEndPoint + "/" + directorId, directorName);
    await this.componentDidMount();
    history.push(`/directors/${directorId}`);
  };

  render() {
    let directorData = this.handleSearch();
    return (
      <div className="container " class="bg ">
        <NavBar />
        <Route
          path="/directors/"
          render={props =>
            directorData ? (
              <div className="list-group list-groupo">
                <input
                  onChange={this.handleSearchChange}
                  className="form-control input-normal search"
                  id="search"
                  type="text"
                  placeholder="Search"
                  aria-label="Search"
                />
                {directorData.map(singleData => (
                  <Directors
                    key={singleData.directorId}
                    className="list"
                    onDelete={this.deleteDirector}
                    onSubmit={this.submitEdit}
                    {...props}
                    singleData={singleData}
                  />
                ))}
                <div class="input-group mb-3 " id="add">
                  <input
                    type="text"
                    className="form-control m-1"
                    placeholder="Add New Director"
                    aria-label="Add New Director"
                    aria-describedby="basic-addon2"
                    id="adsearch"
                    value={this.state.addDirector}
                    onChange={this.handleAddChange}
                  />
                  <div className="input-group-append">
                    <button
                      className="btn btn-outline-success m-1"
                      type="button"
                      onClick={this.handleSubmit}
                    >
                      Add New Director
                    </button>
                  </div>
                </div>
              </div>
            ) : (
              <h3>Lodding.....</h3>
            )
          }
        />
        <Route path="/movies/" component={Movies} />
      </div>
    );
  }
}

export default App;

// {contactData.map(singleData => (
//   <ShowContact
//     key={singleData.id}
//     onDelete={this.handleDelete}
//     singleData={singleData}
//     onClick={this.callToToggleAddState}
//   />
//  <div class="list-group">
//   <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>

// (async () => {
//   const rawResponse = await fetch('https://httpbin.org/post', {
//     method: 'POST',
//     headers: {
//       'Accept': 'application/json',
//       'Content-Type': 'application/json'
//     },
//     body: JSON.stringify({a: 1, b: 'Textual content'})
//   });
//   const content = await rawResponse.json();

//   console.log(content);
// })();
