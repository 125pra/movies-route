import React, { Component } from "react";

class EditContact extends Component {
  state = {
    newDirectorName: this.props.singleData.name
  };
  close = history => {
    history.push(`/directors`);
  };
  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleSubmit = () => {
    const newDirectorName = { name: this.state.newDirectorName };
    this.props.onSubmit(
      newDirectorName,
      this.props.singleData.directorId,
      this.props.history
    );
  };

  render() {
    console.log(this.props);
    return (
      <div class="card w-10 editDirector">
        <div class="card-body editcon">
          <input
            className="input m-2"
            name="newDirectorName"
            type="text"
            placeholder="Director Name"
            value={this.state.newDirectorName}
            onChange={this.handleChange}
          />
          <button
            type="button"
            class="btn btn-info btn-sm"
            onClick={this.handleSubmit}
          >
            submit
          </button>
          <button
            type="button"
            class="btn btn-info btn-sm"
            id="closeId"
            onClick={() => this.close(this.props.history)}
            history={this.props.history}
          >
            close
          </button>
        </div>
      </div>
    );
  }
}

export default EditContact;
