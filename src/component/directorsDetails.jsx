import React, { Component } from "react";
import { Route, Link } from "react-router-dom";
import EditContact from "./edit";
class DirectorsDetails extends Component {
  render() {
    return (
      <div className="card" id="card">
        {/* <img src={`https://joeschmoe.io/api/v1/${this.props.singleData.name}`} className="card-img-top-left img" alt="..."/> */}
        <div className="card-body">
          <h6 className="card-title">
            <span>ID: </span>
            {this.props.singleData.directorId}
          </h6>
          <h5 className="card-title">{this.props.singleData.name}</h5>
          <span> Movies<hr/> {this.props.singleData.title.map(item=><p>{item}</p>)} </span>
          <p className="card-text">
            Currently we don't have much information about this director
          </p>
        </div>
        <button
          type="button"
          className="btn btn-danger"
          onClick={() =>
            this.props.onDelete(
              this.props.singleData.directorId,
              this.props.history
            )
          }
        >
          Delete
        </button>
        <Link to={`${this.props.match.url}/edit`}>
          <button type="button" className="btn btn-danger edit">
            Edit
          </button>
        </Link>
        <Route
          path={`${this.props.match.url}/edit`}
          render={props => (
            <EditContact
              onSubmit={this.props.onSubmit}
              singleData={this.props.singleData}
              {...props}
            />
          )}
        />
      </div>
    );
  }
}

export default DirectorsDetails;
