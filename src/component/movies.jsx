/* eslint-disable jsx-a11y/anchor-has-content */
import React, { Component } from "react";
import axios from "axios";
import { Route, Link } from "react-router-dom";

import MoviesDetails from "./movieDetails";
import AddMovie from './addMovie'

const movieApiEndPoint = "http://localhost:4500/api/movies";
class Movies extends Component {
  state = {
    moviesData: [],
    searchWord: "",
    addMovie: "",
    movieDetails:{"title":"",
    "rank":0,
    "description":"",
    "runtime":0,
    "genre":"",
    "rating":0,
    "metascore":0,
    "votes":0,
    "grossEarningInMil":0,
    "actor":"",
    "year":0,
    "director":""
    },
  };

  async componentDidMount() {
    const { data: moviesData } = await axios.get(movieApiEndPoint);
    this.setState({ moviesData: moviesData });
  }

  handleDelete=async(movieId,history)=>{
    const orignalDirectorsDatt = this.state.moviesData;
    let moviesData = this.state.moviesData;
    moviesData = moviesData.filter(
      singleData => singleData.movieId !== movieId
    );
    this.setState({ moviesData });
    history.push("/movies");
    try {
      await axios.delete(movieApiEndPoint + "/" + movieId);
    } catch (err) {
      console.log("somthing went wrong");
      this.setState({ moviesData: orignalDirectorsDatt });
    }
   
  }

  handleClose=(history)=>{
    history.push("/movies")
  }

  handleSearchChange = event => {
    let searchWord = event.target.value;
    this.setState({ searchWord });
  };

  handleSearch = () => {
    const moviesData = this.state.moviesData.filter(movie =>
      movie.title.toLowerCase().includes(this.state.searchWord.toLowerCase())
    );
    return moviesData;
  };


  handleSubmit=async(movieDetails,history)=>{
    console.log(movieDetails,history)
     try {
       await axios.post(movieApiEndPoint,movieDetails);
       await this.componentDidMount();
       this.props.history.go(-1)
     } catch (err) {
       alert(err);
     }
  }

  moviesTitle = singleMovie => {
    return (
      <React.Fragment>
        <div className="linkDiv">
          <Link
            to={`${this.props.match.url}/${singleMovie.movieId}`}
            className="list-group-item list-group-item-action list-group-item-warning list"
            id="list"
          >
            {singleMovie.title}
          </Link>
        </div>
        <div className="routeDiv">
          <Route
            path={`${this.props.match.url}/${singleMovie.movieId}`}
            render={props => (
              <MoviesDetails
                onSubmit={this.props.onSubmit}
                singleMovie={singleMovie}
                onDelete={this.handleDelete}
                onClose={this.handleClose}
                {...props}
              />
            )}
          />
        </div>
      </React.Fragment>
    );
  };

  render() {
    const moviesData = this.handleSearch();
    // console.log(this.props);
    return (
      <React.Fragment>
        <input
          onChange={this.handleSearchChange}
          className="form-control input-normal movieSearch mt-2"
          id="movieSearch"
          type="text"
          placeholder="Search"
          aria-label="Search"
        />
         <Link to={`${this.props.match.url}/add`}>
          <button type="button" className="btn btn-success add">
            Add New Movie
          </button>
        </Link>
        <Route
          path={`${this.props.match.url}/add`}
          render={props => (
            <AddMovie
              onSubmit={this.handleSubmit}
              onClose={this.handleClose}
              {...props}
              moviesData={this.state.moviesData}
              movieDetails={this.state.movieDetails}
            />
          )}
        />
        <div className="movieDiv mb-4">
          {moviesData.map(singleMovie => this.moviesTitle(singleMovie))}
        </div>
      </React.Fragment>
    );
  }
}

export default Movies;
