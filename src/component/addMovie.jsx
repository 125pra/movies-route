import React, { Component } from "react";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
class AddMovie extends Component {

  //   VALUES ("${movieDetails.rank}","${movieDetails.title}" ,"${movieDetails.description}","${movieDetails.runtime}","${movieDetails.genre}","${movieDetails.rating}","${movieDetails.metascore}","${movieDetails.votes}","${movieDetails.grossEarningInMil}","${movieDetails.actor}", "${movieDetails.year}","${directorid}");`

  // handleChange = event => {
  //   this.setState({
  //     [event.target.name]: event.target.value
  //   });
  // };
  
  handleSubmit=async (event)=>{
  // console.log(event.target.parentNode.childNodes[11].childNodes[1].childNodes[0].innertext)
  event.preventDefault();
  console.log(event.target.parentNode.childNodes)
    this.props.movieDetails["rank"]=event.target.parentNode.childNodes[1].value;
    this.props.movieDetails["title"]=event.target.parentNode.childNodes[0].value;
     this.props.movieDetails["description"]=event.target.parentNode.childNodes[2].value;
     this.props.movieDetails["runtime"]=event.target.parentNode.childNodes[3].value;
     this.props.movieDetails["genre"]=event.target.parentNode.childNodes[4].value;
     this.props.movieDetails["rating"]=event.target.parentNode.childNodes[5].value;            
     this.props.movieDetails["metascore"]=event.target.parentNode.childNodes[6].value;         
     this.props.movieDetails["votes"]=event.target.parentNode.childNodes[7].value;
     this.props.movieDetails["grossEarningInMil"]=event.target.parentNode.childNodes[8].value;
     this.props.movieDetails["actor"]=event.target.parentNode.childNodes[9].value;
     this.props.movieDetails["year"]=event.target.parentNode.childNodes[10].value;
    //  movieDetails["name"]= 
  //  console.log(event)
   await  this.props.onSubmit(this.props.movieDetails,this.props.history)

  }

   handleDropDown=(DirectorName)=>{
     let director=""+DirectorName;
    this.props.movieDetails["director"]=director
   }

// {
//   "actor": "ljkljkl",
// "description": "llii",
// "genre": "lll",
// "grossEarningInMil": "99",
// "metascore": "99",
// "rank": "99",
// "rating": "99",
// "runtime": "99",
// "title": "ppp",
// "votes": "99",
// "year": "2019",
// "director":"Alfred Hitchcock"
//     }
  

//    const validateMovie = joi.object().keys({
//     rank: joi.text().min(0).max(100).required(),
//     title: joi.string().min(3).max(30).required(),
//     description: joi.string().min(3).max(1000).required(),
//     runtime: joi.text().min(0).max(10000).required(),
//     genre: joi.string().min(3).max(30).required(),
//     rating: joi.text().min(0).max(100).required(),
//     metascore: joi.text().min(0).max(100000).required(),
//     votes: joi.text().min(0).max(10000000).required(),
//     grossEarningInMil: joi.text().min(0).max(10000).required(),
//     director: joi.string().min(0).max(100).required(),
//     actor: joi.string().min(3).max(40).required(),
//     year: joi.text().min(1000).max(3000).required(),
// });
  render() {
    console.log(this.props)
    return (
      <div class="card addcard" id="addcard">
        <div class="card-body">
          <form  className="contact">
            <div id="movieform">
              <input
                className="input m-2"
                name="title"
                type="text"
                placeholder="title"
                // value={this.state.title}
                // onChange={this.handleChange}
              />
              <input
                className="input m-3 inputadd "
                name="rank"
                type="number" 
                placeholder="rank"
                // onChange={this.handleChange}
                // value={this.state.first_name}
              />

              <input
                className="input m-2"
                name="description"
                type="text"
                placeholder="description"
                // value={this.state.description}
                // onChange={this.handleChange}
              />
              <input
                className="input m-2"
                name="runtime"
                type="number"
                placeholder="runtime"
                // value={this.state.runtime}
                // onChange={this.handleChange}
              />
              <input
                className="input m-2"
                name="genre"
                type="text"
                placeholder="genre"
                // value={this.state.genre}
                // onChange={this.handleChange}
              />
              <input
                className="input m-2"
                name="rating"
                type="number"
                placeholder="rating"
                // value={this.state.rating}
                // onChange={this.handleChange}
              />
              <input
                className="input m-2"
                name="metascore"
                type="number"
                placeholder="metascore"
                // value={this.state.metascore}
                // onChange={this.handleChange}
              />
              <input
                className="input m-2"
                name="votes"
                type="number"
                placeholder="votes"
                // value={this.state.votes}
                // onChange={this.handleChange}
              />
              <input
                className="input m-2"
                name="grossEarningInMil"
                type="text"
                placeholder="grossEarningInMil "
                // value={this.state.metascore}
                // onChange={this.handleChange}
              />
              <input
                className="input m-2"
                name="actor"
                type="text"
                placeholder="actor"
                // value={this.state.actor}
                // onChange={this.handleChange}
              />
              <input
                className="input m-2"
                name="year"
                type="number"
                placeholder="year"
                // value={this.state.year}
                // onChange={this.handleChange}
              />
              {/* { <DropdownButton id="dropdown-item-button" title="Director_name">
                  {["name","ji","jjjj"].map((m) => 
                // eslint-disable-next-line no-unused-expressions
                <Dropdown.Item as="button">{m}</Dropdown.Item>      
                  )}
              </DropdownButton> } */
              <DropdownButton
              alignRight
              title="Director_name"
              id="dropdown-menu-align-right dropdown"

            >
              {this.props.moviesData.map(m=><Dropdown.Item eventKey={m.movieId}
               className="directorName"
               onClick={()=>this.handleDropDown(m.name)}
              >{m.name}</Dropdown.Item>)}
            </DropdownButton>
              }
              <input
                type="submit"
                className="btn btn-info m-2"
                value="Submit"
                onClick={this.handleSubmit}
              />
            </div>
          </form>
          <button
            type="button"
            class="btn btn-info btn-sm m-2"
            id="closeadd"
            onClick={() => this.props.onClose(this.props.history)}
          >
            Close
          </button>
        </div>
      </div>
    );
  }
}

export default AddMovie;
