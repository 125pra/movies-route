import React, { Component } from "react";
import { Route, Link } from "react-router-dom";
import DirectorsDetails from "./directorsDetails";
class Directors extends Component {
  render() {
    return (
      <React.Fragment>
        <Link
          to={`${this.props.match.url}/${this.props.singleData.directorId}`}
          className="list-group-item list-group-item-action list-group-item-warning list"
          id="list"
        >
          {this.props.singleData.name}
        </Link>
        <Route
          path={`${this.props.match.url}/${this.props.singleData.directorId}`}
          render={props => (
            <DirectorsDetails
              onDelete={this.props.onDelete}
              onSubmit={this.props.onSubmit}
              singleData={this.props.singleData}
              {...props}
            />
          )}
        />
      </React.Fragment>
    );
  }
}

export default Directors;
