import React, { Component } from "react";
import { Route, Link } from "react-router-dom";

class MoviesDetails extends Component {
    state = {  }
    render()
    { 
        console.log(this.props)
        return ( <div class="card">
          <div class="card-body">
          <h5 class="card-title">Title: {this.props.singleMovie.title}</h5>
          <p class="card-text">Actor: {this.props.singleMovie.actor}</p>
          <p class="card-text">description: {this.props.singleMovie.description}</p>
          <p class="card-text">Genre: {this.props.singleMovie.genre}</p>
          <p class="card-text">GrossEarningInMil: {this.props.singleMovie.grossEarningInMil}</p>
          <p class="card-text">Retascore: {this.props.singleMovie.metascore}</p>
          <p class="card-text">Rank: {this.props.singleMovie.rank}</p>
          <p class="card-text">Runtime: {this.props.singleMovie.runtime}</p>
          <p class="card-text">year: {this.props.singleMovie.year}</p>
          <p class="card-text">Director_name: {this.props.singleMovie.name}</p>
          <button type="button" class="btn btn-danger btn-sm m-2" id="delete"
           onClick={() =>
            this.props.onDelete(
              this.props.singleMovie.movieId,
              this.props.history
            )
          }
          >delete</button>
          <button type="button" class="btn btn-info btn-sm m-2" id="close"
          onClick={() =>
            this.props.onClose(
              this.props.history
            )
          }
          >Close</button>
        </div>
      </div>);
    }
}
 
export default MoviesDetails;