import React, { Component } from "react";
import { Link } from "react-router-dom";
class NavBar extends Component {
  state = {};
  render() {
    return (
      <nav className="nav purple col s1">
        <div className="nav-wrapper">
          <ul id="nav-mobile" className="left hide-on-med-and-down ul">
            <li className="left-align left">
              <Link to="/movies">Movies</Link>
            </li>
            <li className="right-align right">
              <Link to="/directors">Directors</Link>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default NavBar;
